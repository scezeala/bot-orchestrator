﻿using OrchestratorModel.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using OrchestrationApi.Infrastructure;
using OrchestrationApi.Models;
using System.Threading.Tasks;

namespace OrchestrationApi.Controllers
{
    [RoutePrefix("api/dashboard")]
    public class DashboardController : ApiController
    {
        //Pending Run Per Day
        //Completed Run Per
        //Runs carried above currentday
        //Total Processess
        //Running Processes
        ProcessScheduleQueries processScheduleQueries;
        private readonly IMapper mapper;
        
        public DashboardController()
        {
            processScheduleQueries = new ProcessScheduleQueries();
            mapper = AutomapperWebProfile.mapper;
        }

        [HttpGet]
        [Route("pending")]
        public async Task<IHttpActionResult> Pending()
        {
           
            try
            {
                var process = await processScheduleQueries.GetPendingProcesses();
                var pending =  mapper.Map<List<ProcessScheduleModel>>(process);
                return Ok(pending);
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("pendingcount")]
        public async Task<IHttpActionResult> CountPending()
        {

            try
            {
                var process = await processScheduleQueries.GetPendingProcesses();
                var pending = mapper.Map<List<ProcessScheduleModel>>(process);
                int totalPending = pending.Count();
                return Ok(new
                {
                    count = totalPending
                });
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("completed")]
        public async Task<IHttpActionResult> Complete()
        {

            try
            {
                var process = await processScheduleQueries.GetCompletedProcesses();
                var pending = mapper.Map<List<ProcessScheduleModel>>(process);
                return Ok(pending);
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("completedcount")]
        public async Task<IHttpActionResult> CompletedCount()
        {

            try
            {
                var process = await processScheduleQueries.GetCompletedProcesses();
                var completed = mapper.Map<List<ProcessScheduleModel>>(process);
                int totalCompleted = completed.Count();
                return Ok(new
                {
                    count = totalCompleted
                });
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("delayed")]
        public async Task<IHttpActionResult> Delayed()
        {

            try
            {
                var process = await processScheduleQueries.GetDelayedProcesses();
                var pending = mapper.Map<List<ProcessScheduleModel>>(process);
                return Ok(pending);
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("delayedcount")]
        public async Task<IHttpActionResult> DelayedCount()
        {

            try
            {
                var process = await processScheduleQueries.GetDelayedProcesses();
                var delayed = mapper.Map<List<ProcessScheduleModel>>(process);
                int totalDelayed = delayed.Count();
                return Ok(new
                {
                    count = totalDelayed
                });
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("runningcount")]
        public async Task<IHttpActionResult> RunningProcesses()
        {
            try
            {
                var process = await processScheduleQueries.GetRunningProcesses();
                var runningProcess = mapper.Map<List<ProcessScheduleModel>>(process);
                int totalRunning = runningProcess.Count();
                return Ok(new {
                    count = totalRunning
                });
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("processcount")]
        public async Task<IHttpActionResult> ProcessesCount()
        {
            try
            {
                var process = await processScheduleQueries.GetAll();
                var runningProcess = mapper.Map<List<ProcessScheduleModel>>(process);
                int totalRunning = runningProcess.Count();
                return Ok(new
                {
                    count = totalRunning
                });
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}
