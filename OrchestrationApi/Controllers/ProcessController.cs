﻿using AutoMapper;
using OrchestrationApi.Infrastructure;
using OrchestrationApi.Models;
using OrchestratorModel.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace OrchestrationApi.Controllers
{
    [RoutePrefix("api/process")]
    public class ProcessController : ApiController
    {
        BluePrismQueries bluePrismQueries;
        ProcessScheduleQueries processScheduleQueries;
        private readonly IMapper mapper;
        public ProcessController()
        {
            bluePrismQueries = new BluePrismQueries();
            processScheduleQueries = new ProcessScheduleQueries();
            mapper = AutomapperWebProfile.mapper;
        }

        //Get List of Published Processes
        [HttpGet]
        [Route("published")]
        public async Task<IHttpActionResult> Published()
        {

            try
            {
                var process = await bluePrismQueries.GetProcesses(1);
                var published = mapper.Map<List<BPAProcessModel>>(process);
                return Ok(published);
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }

        //Get List of Unpublished Processes
        [HttpGet]
        [Route("unpublished")]
        public async Task<IHttpActionResult> Unpublished()
        {

            try
            {
                var process = await bluePrismQueries.GetProcesses(2);
                var published = mapper.Map<List<BPAProcessModel>>(process);
                return Ok(published);
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("schedulable")]
        public async Task<IHttpActionResult> Schedulable()
        {

            try
            {
                var process = await bluePrismQueries.GetProcesses(1);
                var published = mapper.Map<List<BPAProcessModel>>(process);

                var scheduled = await processScheduleQueries.GetAll();
                var runningProcess = mapper.Map<List<ProcessScheduleModel>>(scheduled);

                string[] processIds = runningProcess.Select(x => x.ProcessId).ToArray();

                var schedulableProcess = published.Where(x => !processIds.Contains(x.ProcessId)).ToList();

                return Ok(schedulableProcess);
            }
            catch (Exception ex)
            {
                return NotFound();
            }
        }

        [HttpPost]
        [Route("schedule")]
        public async Task<IHttpActionResult> Schedule(ProcessScheduleModel model)
        {
            try
            {
                string Message = string.Empty;
                string Code = string.Empty;

                //Func<string, DateTime,DateTime> getActualStartTime = (a,b) =>
                //{
                //    DateTime startTime = Convert.ToDateTime(a);
                //    DateTime nextRunDate = new DateTime(b.Year, b.Month, b.Day, startTime.Hour, startTime.Minute, startTime.Second, DateTimeKind.Local);
                //    return nextRunDate;
                //};

                Func<string, string,int, bool> checkIfDateIsInRange = (existing, newrecord,totalRunTime) =>
                {
                    DateTime existingTime = Convert.ToDateTime(existing);
                    DateTime endExistingDateTime = existingTime.AddMinutes(totalRunTime);
                    TimeSpan existingTimeSpam = new TimeSpan(existingTime.Hour, existingTime.Minute, existingTime.Second);
                    TimeSpan endExistingTimeSpam = new TimeSpan(endExistingDateTime.Hour, endExistingDateTime.Minute, endExistingDateTime.Second);

                    DateTime newTime = Convert.ToDateTime(newrecord);
                    TimeSpan newTimeSpam = new TimeSpan(newTime.Hour, newTime.Minute, newTime.Second);
                    
                    return existingTimeSpam <= newTimeSpam && newTimeSpam <= endExistingTimeSpam;
                };

                if (ModelState.IsValid)
                {
                    
                    //DateTime schdeuledTime = Convert.ToDateTime(model.ProcessDailyStartTime);
                    //DateTime scheduledDateTime = new DateTime(model.NextRun.Year, model.NextRun.Month, model.NextRun.Day,
                    //    schdeuledTime.Hour, schdeuledTime.Minute, schdeuledTime.Second, DateTimeKind.Local);

                    var scheduled = await processScheduleQueries.GetAll();
                    scheduled = scheduled.Where(m => m.ResourceId == model.ResourceId).ToList();
                    var runningProcess = mapper.Map<List<ProcessScheduleModel>>(scheduled);

                    //runningProcess.ForEach(x => x.NextRun = getActualStartTime(x.ProcessDailyStartTime,x.NextRun));

                   var isOverlappingTime = runningProcess.Any(x => checkIfDateIsInRange(x.ProcessDailyStartTime, model.ProcessDailyStartTime, x.TotalRunningMinutes));
                    if (!isOverlappingTime)
                    {
                        var resource = await bluePrismQueries.GetResource(model.ResourceId);
                        string[] resourceParts = resource.Name.Split(':');
                        model.Resource = resourceParts[0];
                        model.Port = (resourceParts.Length > 1) ? resourceParts[0] : "";
                    }
                    else
                    {
                        Message = "Another process is scheduled to run within the time range for selected resource";
                        Code = "99";
                    }
                    //var  = runningProcess.OrderByDescending().
                }
                else
                {
                    var errorList = (from item in ModelState.Values
                                     from error in item.Errors
                                     select error.ErrorMessage).ToList();
                    foreach (var err in errorList)
                    {
                        Message += $" {err.ToLower()} {Environment.NewLine},";
                    }
                    Code = "99";


                }

                return Ok(new
                {
                    code = Code,
                    msg = Message
                });
            }
            catch (Exception ex)
            {

                return StatusCode(HttpStatusCode.InternalServerError);
            }
        }

        //Get StartUp Properties per Process

        //Schedule new process
        //Enable Or Disable Process
        //View Process Details
    }
}
