﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using OrchestrationApi.Models;
using OrchestratorModel.Model;

namespace OrchestrationApi.Infrastructure
{
    public class AutomapperWebProfile : Profile
    {
        public static IMapper mapper { get; private set; }
        public static void Run()
        {
            var mapperConfiguration = new MapperConfiguration(config =>
            {
                config.AddProfile(new AutomapperWebProfile());
            });

            mapper = mapperConfiguration.CreateMapper();
        }

        public AutomapperWebProfile()
        {
            CreateMap<BotSession, BotSessionModel>().ReverseMap();
            CreateMap<ProcessSchedule, ProcessScheduleModel>().ReverseMap();
            CreateMap<StartUpParam, StartUpParamModel>().ReverseMap();
            CreateMap<BPAProcess, BPAProcessModel>().ReverseMap();
            CreateMap<BPAResource, BPAResourceModel>().ReverseMap();
        }
    }
}