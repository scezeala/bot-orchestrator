﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrchestrationApi.Models
{
    public class BotSessionModel
    {
        public int Id { get; set; }
        public string ProcessId { get; set; }
        public string ProcessName { get; set; }
        public DateTime RunTime { get; set; }
        public string SessionId { get; set; }
    }
}