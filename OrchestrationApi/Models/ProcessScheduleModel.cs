﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OrchestrationApi.Models
{
    public class ProcessScheduleModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        [Required(ErrorMessage ="Select Process")]
        public string ProcessId { get; set; }
        [Required(ErrorMessage = "Specify Multi Run Frequency in Minutes")]
        public int MultiRunFrequency { get; set; }
        [Required(ErrorMessage = "Specify Next Run Date and Time")]
        [DataType(DataType.DateTime)]
        public DateTime NextRun { get; set; }
        [Required(ErrorMessage = "Specify Daily Start Time.")]
        [RegularExpression(@"[0-9]{2}:[0-9]{2}$", ErrorMessage = "Time should be in 24 hrs foramt e.g 15:35 ")]
        public string ProcessDailyStartTime { get; set; }
        [Required(ErrorMessage = "Specify If process should run on a weekend.")]
        public bool RunWeekend { get; set; }
        [Required(ErrorMessage = "Specify Settlement or Reconciliation Date.")]
        public DateTime SettlementOrReconDate { get; set; }
        [Required(ErrorMessage = "Specify Settlement or Reconciliation Date should change on process failure")]
        public bool SettlementOrReconDateOnFail { get; set; }
        public int Session { get; set; }
        public string EmailList { get; set; }
        public string Resource { get; set; }
        [Required(ErrorMessage = "Specify Resource to Run this Process")]
        public string ResourceId { get; set; }
        public string Port { get; set; }
        public string Status { get; set; }
        public string Inputs { get; set; }
        public bool ProcessState { get; set; }
        [Required(ErrorMessage = "Specify Maximun time it takes the process to complete")]
        public int TotalRunningMinutes { get; set; }
    }
}