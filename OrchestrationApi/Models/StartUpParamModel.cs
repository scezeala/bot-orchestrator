﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrchestrationApi.Models
{
    public class StartUpParamModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ProcessId { get; set; }
        public string InputParam { get; set; }
        public string InputValue { get; set; }
    }
}