﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrchestrationService
{
    public static class Extensions
    {
        public static string GetKeyValue(this string key)
        {
            return ConfigurationManager.AppSettings[key].ToString();
        }

        public static void Dump(this string word)
        {
            Console.WriteLine(word);
        }
    }
}
