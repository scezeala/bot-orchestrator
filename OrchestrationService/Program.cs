﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrchestratorModel.Query;
using OrchestratorModel.Model;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Xml;
using System.Reflection;

namespace OrchestrationService
{
    class Program
    {
        static Logger logger = new Logger();
        static ProcessScheduleQueries queries = new ProcessScheduleQueries();
        static BotSessionQueries sessionQueries = new BotSessionQueries();
        static BluePrismQueries bluePrismQueries = new BluePrismQueries();
        static StartUpParamQueries startUpParamQueries = new StartUpParamQueries();
        static async Task Main(string[] args)
        {
            bool AutoRun = true;
            int TotalBot = Convert.ToInt32("totalbot".GetKeyValue());
            // string processxml = await bluePrismQueries.GetProcessInfo();
            var date = DateTime.Now;
            while (AutoRun)
            {
                var currentTime = DateTime.Now;
                int ActiveSessions = await bluePrismQueries.GetTotalRunningBot();
                $"{currentTime} ::: Active Sessions Running::: {ActiveSessions}".Dump();
                if (TotalBot > ActiveSessions)
                {
                    var getNextRun = await queries.GetNextRun();
                    bool runProcess = false;
                    if (getNextRun != null)
                    {
                        logger.Info($"Checking Resource Availability for {getNextRun.Name} ResourceID:::{getNextRun.ResourceId}");
                        int resourceCheck = await bluePrismQueries.CheckResourceState(getNextRun.ResourceId.Trim());
                        if(resourceCheck == 0)
                        {
                            var DateDifference = currentTime - getNextRun.NextRun;
                            decimal computeProcessingHours = Convert.ToDecimal(DateDifference.TotalHours);
                            if (computeProcessingHours > 0)
                            {
                                if (currentTime.DayOfWeek == DayOfWeek.Saturday
                                    || currentTime.DayOfWeek == DayOfWeek.Sunday)
                                {
                                    runProcess = getNextRun.RunWeekend ? true : false;
                                }
                                else
                                {
                                    runProcess = true;
                                }
                            }
                        }
                        else
                        {
                            logger.Info($"Resource Busy for {getNextRun.Name} ResourceID:::{getNextRun.ResourceId}");
                        }
                       
                    }

                    if (runProcess)
                    {
                        var startUpParams = await ExtractXml(getNextRun.Inputs, getNextRun.ProcessId.Trim(), getNextRun.Id);
                        string arguments = BuildCommand(getNextRun.Name, getNextRun.Resource, getNextRun.Port, startUpParams);
                        string output = StartProcess(arguments);
                        output.Dump();
                        if (!string.IsNullOrEmpty(output))
                        {
                            await queries.UpdateStatus(getNextRun.ProcessId.Trim());
                            await sessionQueries.InsertRecord(new BotSession
                            {
                                ProcessId = getNextRun.ProcessId,
                                ProcessName = getNextRun.Name,
                                RunTime = DateTime.Now,
                                SessionId = output
                            });
                        }
                           
                        runProcess = false;
                    }
                }
                else
                {
                    $"{currentTime} ::: Total Bot Utilized::: {TotalBot}/{TotalBot}".Dump();
                }

                await Task.Delay(12000);
                
            }
           
           

           


            

           
            

            Console.ReadLine();

            //int addRecord = await queries.InsertRecord(new ProcessSchedule()
            //{
            //    Code = "CircularPath",
            //    EmailList = "sezeala@gmail.com",
            //    MultiRunFrequency = 60,
            //    Name = "circular path",
            //    NextRun = DateTime.Now.AddMinutes(20),
            //    ProcessDailyStartTime = "09:00",
            //    Port = "",
            //    ProcessId = "502C3FFF-8DA1-4850-8AD7-69ABFD0E245C",
            //    Resource = "ESC2581",
            //    RunWeekend = true,
            //    SettlementOrReconDate = DateTime.Now.AddDays(-1),
            //    Session = 1,
            //    SettlementOrReconDateOnFail = true,
            //    Inputs = processxml
            //});
        }

        public static string BuildCommand(string processName, string ResouceName, string port, string startups)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" /run");
            sb.Append($" \"{processName}\" ");
            sb.Append($"/resource {ResouceName}");
            if (!string.IsNullOrEmpty(port))
                sb.Append($" /port {port}");
            sb.Append($" /dbconname \"{"dbconname".GetKeyValue()}\" ");
            sb.Append($"/user {"user".GetKeyValue()} {"password".GetKeyValue()}");
            if (!string.IsNullOrEmpty(startups))
                sb.Append($" /startp \"{startups}\"");

            return sb.ToString();

        }

        public static string StartProcess(string args)
        {
            string output = string.Empty;
            string workingFolder = "workingFolder".GetKeyValue();
            using (Process myProcess = new Process())
            {
                myProcess.StartInfo.FileName = workingFolder;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.StartInfo.Arguments = args;
                // myProcess.StartInfo.RedirectStandardInput = true;
                myProcess.StartInfo.RedirectStandardOutput = true;
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.Start();
                myProcess.WaitForExit();
                //myProcess.StandardInput.WriteLine(args);
                //myProcess.StandardInput.Flush();
                //myProcess.StandardInput.Close();
                // myProcess.WaitForExit();
                string result = myProcess.StandardOutput.ReadToEnd();
                result.Dump();
                if (string.IsNullOrEmpty(result))
                    return null;
                output = ExtractSessionId(result);
               // Console.WriteLine(myProcess.StandardOutput.ReadToEnd());
               
                // You can start any process, HelloWorld is a do-nothing example.


               // myProcess.StartInfo.Arguments = args;
               
               

                // This code assumes the process you are starting will terminate itself.
                // Given that is is started without a window so you cannot terminate it
                // on the desktop, it must terminate itself or you can do it programmatically
                // from this application using the Kill method.
            }

            return output;
        }

        public static string ExtractSessionId(string result)
        {
            string output = null;
            string regexPattern = @"^(Session:)(?<session>[A-Za-z0-9\W]*)";
            Regex R = new Regex(regexPattern, RegexOptions.Multiline);
            MatchCollection mc = R.Matches(result);
            if(mc != null && mc.Count > 0)
            {
                foreach (Match m in mc)
                {
                    if (m != null && m.Success)
                    {
                        output = m.Groups["session"].Success ? m.Groups["session"].Value : null;
                    }
                }
            }
            return output;
        }

        public static async Task<string> ExtractXml(string xmlParam, string processId, int id)
        {
            string output = null;
            StringBuilder sb = new StringBuilder();
            if (!string.IsNullOrEmpty(xmlParam))
            {
                try
                {
                    sb.Append("<inputs>");
                    var startParams = await startUpParamQueries.GetAll(processId);
                    var xmldoc = new XmlDocument();
                    xmldoc.LoadXml(xmlParam.Trim());
                   var model = await queries.GetStartUpParameterFromProcessSchedule(id);
                    foreach (var sp in startParams)
                    {
                        XmlNode xnList = xmldoc.SelectSingleNode($"inputs/input[@name='{sp.InputParam.Trim()}']");
                        if (xnList != null)
                        {
                            string value = null;
                            var pp = model.GetType().GetProperty(sp.InputValue).GetValue(model, null);
                          var type =   pp.GetType();
                            if(type == typeof(DateTime))
                            {
                                var dd = (DateTime)pp;
                                value = dd.ToString("yyyy-MM-dd");
                            }
                            else
                            {
                                value = pp.ToString();
                            }

                            XmlAttribute typeAttr = xmldoc.CreateAttribute("value");
                            typeAttr.Value = sp.InputValue;
                            xnList.Attributes.Append(typeAttr);

                            XmlElement enode = (XmlElement)xnList;
                            //enode.RemoveAttribute("type");
                            enode.RemoveAttribute("stage");
                            sb.Append(enode.OuterXml.Replace("\"", "'"));
                        }
                    }
                    sb.Append("</inputs>");
                    output = sb.ToString();
                }
                catch (Exception ex)
                {

                    //throw;
                }
                

                
               // XmlNodeList xnList = xmldoc.SelectNodes("inputs/input[@name='Start']");
            }
            return output;
        }

       


    }
}
