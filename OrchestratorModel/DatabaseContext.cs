﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrchestratorModel.Model;

namespace OrchestratorModel
{
    class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("DbConnection")
        {

        }

        public DbSet<ProcessSchedule> Schedule { get; set; }
        public DbSet<StartUpParam> StartUpParams { get; set; }
        public DbSet<BotSession> BotSessions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new ProcessScheduleConfig());
            modelBuilder.Configurations.Add(new StartUpParamConfiguration());
            modelBuilder.Configurations.Add(new BotSessionConfiguration());
        }

    }
}
