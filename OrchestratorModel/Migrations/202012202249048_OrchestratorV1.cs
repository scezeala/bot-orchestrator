﻿namespace OrchestratorModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrchestratorV1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProcessSchedules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcessName = c.String(maxLength: 250, unicode: false),
                        ProcessCode = c.String(maxLength: 100, unicode: false),
                        ProcessId = c.String(maxLength: 350, unicode: false),
                        MultiRunFrequency = c.Int(),
                        NextRunDateTime = c.DateTime(nullable: false),
                        ProcessDailyStartTime = c.String(maxLength: 10, unicode: false),
                        RunWeekend = c.Boolean(nullable: false),
                        SettlementOrReconDate = c.DateTime(),
                        SettlementOrReconDateOnFail = c.Boolean(nullable: false),
                        EmailList = c.String(maxLength: 800, unicode: false),
                        Resource = c.String(maxLength: 50, unicode: false),
                        Port = c.String(maxLength: 10, unicode: false),
                        Session = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ProcessSchedules");
        }
    }
}
