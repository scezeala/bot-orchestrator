﻿namespace OrchestratorModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrchestratorV1StatusAdded : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.ProcessSchedules", name: "Port", newName: "ProcessStatus");
            AddColumn("dbo.ProcessSchedules", "Status", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProcessSchedules", "Status");
            RenameColumn(table: "dbo.ProcessSchedules", name: "ProcessStatus", newName: "Port");
        }
    }
}
