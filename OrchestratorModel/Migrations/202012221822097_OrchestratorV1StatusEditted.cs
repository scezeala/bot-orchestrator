﻿namespace OrchestratorModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrchestratorV1StatusEditted : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.ProcessSchedules", name: "ProcessStatus", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.ProcessSchedules", name: "Status", newName: "ProcessStatus");
            RenameColumn(table: "dbo.ProcessSchedules", name: "__mig_tmp__0", newName: "Port");
            AlterColumn("dbo.ProcessSchedules", "ProcessStatus", c => c.String(maxLength: 10, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProcessSchedules", "ProcessStatus", c => c.String());
            RenameColumn(table: "dbo.ProcessSchedules", name: "Port", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.ProcessSchedules", name: "ProcessStatus", newName: "Status");
            RenameColumn(table: "dbo.ProcessSchedules", name: "__mig_tmp__0", newName: "ProcessStatus");
        }
    }
}
