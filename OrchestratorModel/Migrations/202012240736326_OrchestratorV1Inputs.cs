﻿namespace OrchestratorModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrchestratorV1Inputs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProcessSchedules", "ProcessInputXml", c => c.String(maxLength: 4000));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProcessSchedules", "ProcessInputXml");
        }
    }
}
