﻿namespace OrchestratorModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrchestratorV1AddStartUpParam : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StartUpParams",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcessName = c.String(maxLength: 250, unicode: false),
                        ProcessId = c.String(maxLength: 350, unicode: false),
                        InputParam = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.StartUpParams");
        }
    }
}
