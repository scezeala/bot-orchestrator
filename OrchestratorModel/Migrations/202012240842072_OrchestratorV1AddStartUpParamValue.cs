﻿namespace OrchestratorModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrchestratorV1AddStartUpParamValue : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StartUpParams", "InputValue", c => c.String(maxLength: 100, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StartUpParams", "InputValue");
        }
    }
}
