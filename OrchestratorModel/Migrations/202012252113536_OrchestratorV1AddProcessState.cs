﻿namespace OrchestratorModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrchestratorV1AddProcessState : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProcessSchedules", "ProcessState", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProcessSchedules", "ProcessState");
        }
    }
}
