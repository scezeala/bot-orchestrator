﻿// <auto-generated />
namespace OrchestratorModel.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class OrchestratorV1AddSessionTable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(OrchestratorV1AddSessionTable));
        
        string IMigrationMetadata.Id
        {
            get { return "202012262345219_OrchestratorV1AddSessionTable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
