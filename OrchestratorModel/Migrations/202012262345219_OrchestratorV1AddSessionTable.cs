﻿namespace OrchestratorModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrchestratorV1AddSessionTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BotSessions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProcessId = c.String(maxLength: 350, unicode: false),
                        ProcessName = c.String(maxLength: 250, unicode: false),
                        RunTime = c.DateTime(nullable: false),
                        SessionId = c.String(maxLength: 250, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ProcessSchedules", "ResourceId", c => c.String(maxLength: 250, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProcessSchedules", "ResourceId");
            DropTable("dbo.BotSessions");
        }
    }
}
