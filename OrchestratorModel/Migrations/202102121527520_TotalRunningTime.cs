﻿namespace OrchestratorModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TotalRunningTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProcessSchedules", "TotalRunningMinutes", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProcessSchedules", "TotalRunningMinutes");
        }
    }
}
