﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrchestratorModel.Model
{
    public class BPAProcess
    {
       public string ProcessId { get; set; }
       public string ProcessType { get; set; }
       public string Name { get; set; }
       public string Description { get; set; }
       public Nullable<DateTime> CreateDate { get; set; }
       public Nullable<DateTime> LastModifiedDate { get; set; }
       public string ProcessXml { get; set; }
       public string WsPublishName { get; set; }
       public int RunMode { get; set; }
    }
}
