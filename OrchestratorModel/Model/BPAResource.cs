﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrchestratorModel.Model
{
    public class BPAResource
    {
		public string ResourceId { get; set; }
		public string Name { get; set; }
		public int ProcessesRunning { get; set; }
		public int ActionsRunning { get; set; }
		public int UnitsAllocated { get; set; }
		public Nullable<DateTime> LastUpdated { get; set; }
		public int AttributeID { get; set; }

		public string FQDN { get; set; }
		public int StatusId { get; set; }
		public string DisplayStatus { get; set; }
		public string currentculture { get; set; }
	}
}
