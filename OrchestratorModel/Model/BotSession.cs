﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

namespace OrchestratorModel.Model
{
    public class BotSession
    {
        public int Id { get; set; }
        public string ProcessId { get; set; }
        public string ProcessName { get; set; }
        public DateTime RunTime { get; set; }
        public string SessionId { get; set; }
    }

    public class BotSessionConfiguration : EntityTypeConfiguration<BotSession>
    {
        public BotSessionConfiguration()
        {
            HasKey<long>(x => x.Id);
            Property(x => x.Id)
                .HasColumnOrder(1)
                .HasColumnType("int")
                .IsRequired();
            Property(x => x.ProcessId)
               .HasColumnOrder(2)
               .HasColumnType("varchar")
               .HasMaxLength(350)
               .HasColumnName("ProcessId");
            Property(x => x.ProcessName)
                .HasColumnOrder(3)
                .HasColumnType("varchar")
                .HasMaxLength(250)
                .HasColumnName("ProcessName");
            Property(x => x.RunTime)
                .HasColumnOrder(4)
                .HasColumnType("datetime")
                .HasColumnName("RunTime")
                .IsRequired();
            Property(x => x.SessionId)
                .HasColumnOrder(5)
                .HasColumnType("varchar")
                .HasMaxLength(250)
                .HasColumnName("SessionId");
        }
    }
}
