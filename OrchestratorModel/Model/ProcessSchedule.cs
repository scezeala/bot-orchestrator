﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

namespace OrchestratorModel.Model
{
    public class ProcessSchedule
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string ProcessId { get; set; }
        public int MultiRunFrequency { get; set; }
        public DateTime NextRun { get; set; }
        public string ProcessDailyStartTime { get; set; }
        public bool RunWeekend { get; set; }
        public DateTime SettlementOrReconDate { get; set; }
        public bool SettlementOrReconDateOnFail { get; set; }
        public int Session { get; set; }
        public string EmailList { get; set; }
        public string Resource { get; set; }
        public string ResourceId { get; set; }
        public string Port { get; set; }
        public string Status { get; set; }
        public string Inputs { get; set; }
        public bool ProcessState { get; set; }

        public int TotalRunningMinutes { get; set; }
    }

    public class ProcessScheduleConfig : EntityTypeConfiguration<ProcessSchedule>
    {
        public ProcessScheduleConfig()
        {
            HasKey<int>(x => x.Id);
            Property(x => x.Id)
                .HasColumnOrder(1)
                .HasColumnType("int")
                .IsRequired();
            Property(x => x.Name)
                .HasColumnOrder(2)
                .HasColumnType("varchar")
                .HasMaxLength(250)
                .HasColumnName("ProcessName");
            Property(x => x.Code)
                .HasColumnOrder(3)
                .HasColumnType("varchar")
                .HasMaxLength(100)
                .HasColumnName("ProcessCode");
            Property(x => x.ProcessId)
                .HasColumnOrder(4)
                .HasColumnType("varchar")
                .HasMaxLength(350)
                .HasColumnName("ProcessId");
            Property(x => x.MultiRunFrequency)
                .HasColumnOrder(5)
                .HasColumnType("int")
                .IsOptional();
            Property(x => x.NextRun)
                .HasColumnOrder(6)
                .HasColumnType("datetime")
                .HasColumnName("NextRunDateTime")
                .IsRequired();
            Property(x => x.ProcessDailyStartTime)
                .HasColumnOrder(7)
                .HasColumnType("varchar")
                .HasColumnName("ProcessDailyStartTime")
                .HasMaxLength(10);
            Property(x => x.RunWeekend)
                .HasColumnOrder(8)
                .HasColumnType("bit")
                .IsRequired();
            Property(x => x.SettlementOrReconDate)
                .HasColumnOrder(9)
                .HasColumnType("datetime")
                .HasColumnName("SettlementOrReconDate")
                .IsOptional();
            Property(x => x.SettlementOrReconDateOnFail)
                .HasColumnOrder(10)
                .HasColumnType("bit");
            Property(x => x.EmailList)
                .HasColumnOrder(11)
                .HasColumnType("varchar")
                .HasColumnName("EmailList")
                .HasMaxLength(800);
            Property(x => x.Resource)
               .HasColumnOrder(12)
               .HasColumnType("varchar")
               .HasColumnName("Resource")
               .HasMaxLength(50);
            Property(x => x.Port)
               .HasColumnOrder(13)
               .HasColumnType("varchar")
               .HasColumnName("Port")
               .HasMaxLength(10);
            Property(x => x.Status)
               .HasColumnOrder(14)
               .HasColumnType("varchar")
               .HasColumnName("ProcessStatus")
               .HasMaxLength(10);
            Property(x => x.Inputs)
              .HasColumnOrder(15)
              .HasColumnType("nvarchar")
              .HasColumnName("ProcessInputXml")
              .HasMaxLength(4000);
            Property(x => x.ProcessState)
             .HasColumnOrder(16)
             .HasColumnType("bit")
             .HasColumnName("ProcessState");
            Property(x => x.ResourceId)
               .HasColumnOrder(17)
               .HasColumnType("varchar")
               .HasColumnName("ResourceId")
               .HasMaxLength(250);


        }
    }
}

