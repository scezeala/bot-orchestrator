﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

namespace OrchestratorModel.Model
{
    public class StartUpParam
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string ProcessId { get; set; }
        public string InputParam { get; set; }
        public string InputValue { get; set; }
    }

    public class StartUpParamConfiguration : EntityTypeConfiguration<StartUpParam>
    {
        public StartUpParamConfiguration()
        {
            HasKey<int>(x => x.Id);
            Property(x => x.Id)
                .HasColumnOrder(1)
                .HasColumnType("int")
                .IsRequired();
            Property(x => x.Name)
                .HasColumnOrder(2)
                .HasColumnType("varchar")
                .HasMaxLength(250)
                .HasColumnName("ProcessName");
            
            Property(x => x.ProcessId)
                .HasColumnOrder(3)
                .HasColumnType("varchar")
                .HasMaxLength(350)
                .HasColumnName("ProcessId");
            Property(x => x.InputParam)
               .HasColumnOrder(4)
               .HasColumnType("varchar")
               .HasMaxLength(100)
               .HasColumnName("InputParam");
            Property(x => x.InputValue)
              .HasColumnOrder(5)
              .HasColumnType("varchar")
              .HasMaxLength(100)
              .HasColumnName("InputValue");
        }
    }
}
