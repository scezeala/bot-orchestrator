﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Xml;
using OrchestratorModel.Model;

namespace OrchestratorModel.Query
{
    public class BluePrismQueries
    {
        string connectionString;
        public BluePrismQueries()
        {
            connectionString = ConfigurationManager.ConnectionStrings["BPConnection"].ConnectionString;
        }
        public async Task<int> GetTotalRunningBot()
        {
            int ActiveSession = 0;
            string query = "SELECT COUNT(*) ActiveSession FROM BPAResource WHERE processesrunning = 1 and DisplayStatus not in ('Missing', 'Offline')";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);
                connection.Open();
                SqlDataReader reader = await command.ExecuteReaderAsync();
                try
                {
                    
                    while (await reader.ReadAsync())
                    {
                        ActiveSession = reader["ActiveSession"] != DBNull.Value ? (int)reader["ActiveSession"] : 0;
                    }
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    reader.Close();
                    connection.Close();
                }
                return ActiveSession;
            }
        }

        public async Task<string> GetProcessInfo()
        {
            string ActiveSession = "";
            string nodeStrig = null;
            string query = "SELECT processxml FROM BPAProcess WHERE processid = '502C3FFF-8DA1-4850-8AD7-69ABFD0E245C'";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);
                connection.Open();
                SqlDataReader reader = await command.ExecuteReaderAsync();
                try
                {

                    while (await reader.ReadAsync())
                    {
                        var xmldoc = new XmlDocument();
                        ActiveSession = reader["processxml"] != DBNull.Value ? (string)reader["processxml"] : "";
                        xmldoc.LoadXml(ActiveSession);
                        XmlNodeList xnList = xmldoc.SelectNodes("/process/stage[@name='Start']");
                        var firstNode = xnList;
                        foreach(XmlNode x  in xnList)
                        {
                            var hasSubsheet = x.SelectSingleNode("subsheetid");
                            if (hasSubsheet == null)
                            {
                                var inputNodes = x.SelectSingleNode("inputs");
                                nodeStrig = inputNodes?.OuterXml;
                            }
                            
                        }
                        
                    }
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    reader.Close();
                    connection.Close();
                }
                return nodeStrig;
            }
        }

        public async Task<int> CheckResourceState(string resourceId)
        {
            int ActiveSession = 0;
            string query = $"SELECT COUNT(*) IsResourceRunning FROM BPAResource WHERE processesrunning = 1 AND resourceid = '{resourceId}' and DisplayStatus not in ('Missing', 'Offline') ";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);
                connection.Open();
                SqlDataReader reader = await command.ExecuteReaderAsync();
                try
                {

                    while (await reader.ReadAsync())
                    {
                        ActiveSession = reader["IsResourceRunning"] != DBNull.Value ? (int)reader["IsResourceRunning"] : 0;
                    }
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    reader.Close();
                    connection.Close();
                }
                return ActiveSession;
            }
        }

        public async Task<List<BPAProcess>> GetProcesses(int type)
        {
            List<BPAProcess> bpaProcesses = new List<BPAProcess>();
            string query = $@"SELECT cast([processid] as varchar(62)) processid
                                  ,[ProcessType]
                                  ,[name]
                                  ,[description]
                                  ,[createdate]
                                  ,[lastmodifieddate]
                                  ,[processxml]
                                  ,[wspublishname]
                                  ,[runmode]
                              FROM [Blue Prism].[dbo].[BPAProcess] WHERE [ProcessType] = 'P'";
            if(type == 1)
                query = query + " AND  ([AttributeID] & 2) != 0";
            if(type == 2)
                query = query + " AND  ([AttributeID] & 2) = 0";

            Nullable<DateTime> emptyTime = null;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);
                connection.Open();
                SqlDataReader reader = await command.ExecuteReaderAsync();
                try
                {

                    while (await reader.ReadAsync())
                    {
                        BPAProcess bpaProcess = new BPAProcess();
                        bpaProcess.ProcessId = reader["processid"] != DBNull.Value ? (string)reader["processid"] : null;
                        bpaProcess.ProcessType = reader["ProcessType"] != DBNull.Value ? (string)reader["ProcessType"] : null;
                        bpaProcess.Name = reader["name"] != DBNull.Value ? (string)reader["name"] : null;
                        bpaProcess.Description = reader["description"] != DBNull.Value ? (string)reader["description"] : null;
                        bpaProcess.CreateDate = reader["createdate"] != DBNull.Value ? Convert.ToDateTime(reader["createdate"]) : emptyTime;
                        bpaProcess.LastModifiedDate = reader["lastmodifieddate"] != DBNull.Value ? Convert.ToDateTime(reader["lastmodifieddate"]) : emptyTime;
                        bpaProcess.ProcessXml = reader["processxml"] != DBNull.Value ? (string)reader["processxml"] : null;
                        bpaProcess.WsPublishName = reader["wspublishname"] != DBNull.Value ? (string)reader["wspublishname"] : null;
                        bpaProcess.RunMode = reader["runmode"] != DBNull.Value ? (int)reader["runmode"] : 0;
                        bpaProcesses.Add(bpaProcess);
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }
                finally
                {
                    reader.Close();
                    connection.Close();
                }
                return bpaProcesses;
            }


        }

        public async Task<List<BPAResource>> GetResources()
        {
            List<BPAResource> bpaResources = new List<BPAResource>();
            Nullable<DateTime> emptyTime = null;
            string query = @"SELECT cast([resourceid] as varchar(62)) as resourceid
                          ,[name]
                          ,[processesrunning]
                          ,[actionsrunning]
                          ,[unitsallocated]
                          ,[lastupdated]
                          ,[AttributeID]
                          ,[FQDN]
                          ,[statusid]
                          ,[DisplayStatus]
                          
                      FROM [Blue Prism].[dbo].[BPAResource] where AttributeID = 0 and (statusid & 1) <> 0 ";
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(query, connection);
                    connection.Open();
                    SqlDataReader reader = await command.ExecuteReaderAsync();
                    while (await reader.ReadAsync())
                    {
                        BPAResource bpaResource = new BPAResource();
                        bpaResource.ResourceId = reader["resourceid"] != DBNull.Value ? (string)reader["resourceid"] : null;
                        bpaResource.Name = reader["name"] != DBNull.Value ? (string)reader["name"] : null;
                        bpaResource.ActionsRunning = reader["actionsrunning"] != DBNull.Value ? (int)reader["actionsrunning"] : 0;
                        bpaResource.UnitsAllocated = reader["unitsallocated"] != DBNull.Value ? (int)reader["unitsallocated"] : 0;
                        bpaResource.LastUpdated = reader["lastupdated"] != DBNull.Value ? Convert.ToDateTime(reader["lastupdated"]) : emptyTime;
                        bpaResource.AttributeID = reader["AttributeID"] != DBNull.Value ? (int)(reader["AttributeID"]) : 0;
                        bpaResource.FQDN = reader["FQDN"] != DBNull.Value ? (string)reader["FQDN"] : null;
                        bpaResource.StatusId = reader["statusid"] != DBNull.Value ? (int)reader["statusid"] : 0;
                        bpaResource.DisplayStatus = reader["DisplayStatus"] != DBNull.Value ? (string)reader["DisplayStatus"] : null;
                        bpaResource.currentculture =  null;
                        bpaResources.Add(bpaResource);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return bpaResources;
        }

        public async Task<BPAResource> GetResource(string resourceid)
        {
            var resources = await GetResources();
            var resource = resources.FirstOrDefault(x => x.ResourceId.Trim() == resourceid);
            return resource;
        }

    }
}
