﻿using OrchestratorModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrchestratorModel.Query
{
    public class BotSessionQueries
    {
        public async Task<int> InsertRecord(BotSession model)
        {
            int output = 0;
            using (DatabaseContext context = new DatabaseContext())
            {
                context.BotSessions.Add(model);
                output = await context.SaveChangesAsync();
            }
            return output;
        }
    }
}
