﻿using OrchestratorModel.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrchestratorModel.Query
{
    public class ProcessScheduleQueries
    {
        public async Task<int> InsertRecord(ProcessSchedule model)
        {
            int output = 0;
            using(DatabaseContext context = new DatabaseContext())
            {
                 context.Schedule.Add(model);
                output = await context.SaveChangesAsync();
            }
            return output;
        }

        public async Task<ProcessSchedule> GetById(int Id)
        {
            ProcessSchedule model = null;
            using(DatabaseContext context = new DatabaseContext())
            {
                model = await context.Schedule.FirstOrDefaultAsync(x => x.Id == Id);
            }

            return model;
        }

        public async Task<ProcessSchedule> GetNextRun()
        {
            ProcessSchedule model = null;
            using (DatabaseContext context = new DatabaseContext())
            {
                model = await context.Schedule
                            .Where(x => x.ProcessState == true)
                            .Where(x => x.Status != "RUNNING")
                            .OrderBy(x => x.NextRun)
                            .FirstOrDefaultAsync();
            }

            return model;
        }

        public async Task UpdateStatus(string processId)
        {
            using (DatabaseContext context = new DatabaseContext())
            {
                var model = await context.Schedule
                            .Where(x => x.ProcessId == processId)
                            .FirstOrDefaultAsync();
                model.Status = "RUNNING";
                await context.SaveChangesAsync();
            }
        }

        public async Task<List<ProcessSchedule>> GetAll()
        {
            List<ProcessSchedule> model = new List<ProcessSchedule> ();
            using (DatabaseContext context = new DatabaseContext())
            {
                model = await context.Schedule.ToListAsync();
            }
            return model;
        }

        
        public async Task<ProcessSchedule> GetStartUpParameterFromProcessSchedule(int processScheduleId)
        {
            ProcessSchedule model = new ProcessSchedule();
            using (DatabaseContext context = new DatabaseContext())
            {
                model = await context.Schedule.FirstOrDefaultAsync(x => x.Id == processScheduleId);
            }
            return model;
        }

        public async Task<List<ProcessSchedule>> GetPendingProcesses()
        {
            DateTime currentDate = DateTime.Now.Date;
            List<ProcessSchedule> model = new List<ProcessSchedule>();
            using (DatabaseContext context = new DatabaseContext())
            {
                model = await context.Schedule
                    .Where(x => x.NextRun.Year == currentDate.Year
                    && x.NextRun.Month == currentDate.Month
                    && x.NextRun.Day == currentDate.Day)
                    .Where(x => x.Status.Trim() != "RUNNING")    
                    .ToListAsync();
            }
            return model;
        }

        public async Task<List<ProcessSchedule>> GetCompletedProcesses()
        {
            DateTime currentDate = DateTime.Now.Date;
            List<ProcessSchedule> model = new List<ProcessSchedule>();
            using (DatabaseContext context = new DatabaseContext())
            {
                model = await context.Schedule
                    .Where(x => x.NextRun.Date > currentDate)
                    .Where(x => x.Status.Trim() != "RUNNING")
                    .ToListAsync();
            }
            return model;
        }

        public async Task<List<ProcessSchedule>> GetDelayedProcesses()
        {
            DateTime currentDate = DateTime.Now.Date;
            List<ProcessSchedule> model = new List<ProcessSchedule>();
            using (DatabaseContext context = new DatabaseContext())
            {
                model = await context.Schedule
                    .Where(x => x.NextRun.Date < currentDate)
                    .Where(x => x.Status.Trim() != "RUNNING")
                    .ToListAsync();
            }
            return model;
        }

        public async Task<List<ProcessSchedule>> GetRunningProcesses()
        {
            DateTime currentDate = DateTime.Now.Date;
            List<ProcessSchedule> model = new List<ProcessSchedule>();
            using (DatabaseContext context = new DatabaseContext())
            {
                model = await context.Schedule
                    .Where(x => x.Status.Trim() == "RUNNING")
                    .ToListAsync();
            }
            return model;
        }


    }
}
