﻿using OrchestratorModel.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrchestratorModel.Query
{
    public class StartUpParamQueries
    {
        public async Task<List<StartUpParam>> GetAll(string processId)
        {
            List<StartUpParam> model = new List<StartUpParam>();
            using (DatabaseContext context = new DatabaseContext())
            {
                model = await context.StartUpParams.Where(x => x.ProcessId.Trim() == processId).ToListAsync();
            }
            return model;
        }
    }
}
