USE [RPA]
GO
/****** Object:  StoredProcedure [dbo].[sp_process_scheduler]    Script Date: 5/31/2021 7:42:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Samuel Ezeala
-- Create date: 28 December 2020
-- Description:	Update the bot schedule Table
-- =============================================
ALTER PROCEDURE [dbo].[sp_process_scheduler] 
	-- Add the parameters for the stored procedure here
	@processId varchar(250), 
	@retry bit
AS
declare @multiRun int, @nextRunDate DateTime, @processDailyStartTime varchar(10), @settlementDate DateTime, @moveSettlementDate bit,
			@newRunDateOnFail DateTime, @newRunDateDiffFromCurrDate int;
BEGIN
	select @multiRun = MultiRunFrequency, @nextRunDate = NextRunDateTime, @processDailyStartTime = ProcessDailyStartTime,@settlementDate = SettlementOrReconDate,
		@moveSettlementDate = SettlementOrReconDateOnFail from [RPA].[dbo].[ProcessSchedules] where ProcessId = ltrim(rtrim(@processId))
	if @retry = 1
		begin
			select @newRunDateOnFail = dateadd(minute, @multiRun, getdate());
		   select @newRunDateDiffFromCurrDate = DATEDIFF(DAY,convert(DATE,@newRunDateOnFail),convert(date,getdate())) 
		   if @newRunDateDiffFromCurrDate = 0
				begin 
					update [RPA].[dbo].[ProcessSchedules] set NextRunDateTime = @newRunDateOnFail, ProcessStatus = '' where ProcessId = ltrim(rtrim(@processId));
				end
			else
				begin
					if @moveSettlementDate = 1
						begin
							update [RPA].[dbo].[ProcessSchedules] set SettlementOrReconDate = dateadd(DAY,1,@settlementDate) where ProcessId = ltrim(rtrim(@processId));
						end;
					--Reset Run time
					update [RPA].[dbo].[ProcessSchedules] set  ProcessStatus = '', NextRunDateTime = dateadd(day,1, dateadd(hour,
						convert(int,SUBSTRING(LTRIM(RTRIM(@processDailyStartTime)),1,2)),
						dateadd(minute,
							convert(int,SUBSTRING(LTRIM(RTRIM(@processDailyStartTime)),4,2)),
							CAST(CAST(GETDATE() AS DATE) AS DATETIME)))) 
							where ProcessId = ltrim(rtrim(@processId));
				end;
		end;
	if @retry = 0
		begin 
			update [RPA].[dbo].[ProcessSchedules] set NextRunDateTime = dateadd(day,1, dateadd(hour,
					convert(int,SUBSTRING(LTRIM(RTRIM(@processDailyStartTime)),1,2)),
					dateadd(minute,
						convert(int,SUBSTRING(LTRIM(RTRIM(@processDailyStartTime)),4,2)),
						CAST(CAST(@nextRunDate AS DATE) AS DATETIME)))) 
						where ProcessId = ltrim(rtrim(@processId));
			update [RPA].[dbo].[ProcessSchedules] set ProcessStatus = '', SettlementOrReconDate = dateadd(DAY,1,@settlementDate) where ProcessId = ltrim(rtrim(@processId));
		end;
END
